#!/usr/bin/python

from subprocess import check_output
from subprocess import call
import os
import sys


FNULL = open(os.devnull, 'w')

##main ##
args = sys.argv[1:]

colorless_call = ["ls"]
colorless_call.extend(args)
out = check_output(colorless_call).split('\n')

color_call = ['ls', '--color=always']
color_call.extend(args)
color_out = check_output(color_call).split('\n')

for idx, line in enumerate(color_out):
  filename = line

  if filename.isspace():
    continue

  if call(['getfattr', '-n', 'user.comment', out[idx]], stderr=FNULL, stdout=FNULL ) == 0:
    filename += '(!)'
    #filename = "\033[4;" + filename[2:] + "\033[0m"

  print filename


