BASHUTIL_INST_DIR="${HOME}/shell_utils"

export PATH="${PATH}:${BASHUTIL_INST_DIR}/bin"
export EDITOR=vim


# Eternal bash history.
# ---------------------
# Undocumented feature which sets the size to "unlimited".
# http://stackoverflow.com/questions/9457233/unlimited-bash-history
export HISTFILESIZE=-1
export HISTSIZE=-1
export HISTTIMEFORMAT="%d/%m/%y %T "
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_eternal_history
# Force prompt to write history after every command.
# http://superuser.com/questions/20900/bash-history-loss
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"


#color escapes:
export BOLD_ESCAPE="\e[1m"
export BLUE_ESCAPE="\e[34m"
export YELLOW_ESCAPE="\e[33m"
export RED_ESCAPE="\e[31m"
export END_COLOR_ESCAPE="\e[0m"

alias ls='ls --color=auto'
alias liah='ls --color=auto -liAh'
alias liaht='ls --color=auto -liAht'

#test which grep we have and degrade as necessary. ideally, we'll gave the
# -P flag, which takes perl regexes
if [ "$(echo "asd" | grep -P 'blabla')" != "2" ] ; then
  alias gr='grep -P --color'
elif [ "$(echo "asd" | grep -E 'blabla')" != "2" ] ; then
  alias gr='grep -E --color'
else
  alias gr='grep --color'
fi

#puts the last line of last output into the clipboard
clip(){
    fc -s | tail -n 1 | tr -d '\n' | xclip
}

cl(){
    local LINES_SOURCE=${1:-"fc -s"}
    local LINES_FILE="$(mktemp)"
    local LINE_NUMBER=1

    $LINES_SOURCE > $LINES_FILE

    if [ $(wc -l $LINES_FILE | awk '{print $1}') -ne 1 ]; then
        lessc $LINES_FILE
        echo "Line number to copy?"
        read LINE_NUMBER
    fi

    LINE=$(head -n $LINE_NUMBER $LINES_FILE | tail -n1)
    echo "$LINE" | tr -d '\n' | xclip
    errcho "Copied \"$LINE\" to clipboard"
    rm $LINES_FILE
}
export cl

__cf_old(){
    cl "lastfiles"
}
export cf

cur(){
  cl "lasturls"
}

cleancl(){
    sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" "$@"
}

getLine(){
  local lineNum=$1
  local fileName=$2
  head "$fileName" -n "$lineNum" | tail -n1
}

findProg(){
  echo "$PATH" | tr ':' '\n' | xargs -I mypath find mypath -executable -type f | grep $1
}

alias nsl='sudo netstat -tulpn'

alias res='echo $?'

relpath(){
    realpath "$1" | sed "s@^$PWD/\?@@"
}

herereadelf(){
  FILES="$(find . -iname '*.so' | tr '\n' ' ')"
  for f in $FILES ; do
    if [ "$(file $f | awk '{print $2}')" != "ELF" ] ; then
      continue;
    fi

    readelf $@ $f
  done;
}

herenm(){
  FILES="$(find . -iname '*.so*' | tr '\n' ' ')"
  for f in $FILES ; do
    if [ "$(file $f | awk '{print $2}')" != "ELF" ] ; then
      continue;
    fi

    nm -A -a $@ $f
  done;
}

#create tar archive from installed .deb. $1 is the name of the package
tarFromPkg(){
  dpkg -L $1 | xargs -Ifpath sh -c "if ! file 'fpath' | grep directory ; then echo fpath >> /tmp/packFiles.txt; fi "
  tar -czvf $1-files.tar.gz --files-from /tmp/packFiles.txt
}


gitg(){
  local REFS=""
  if [ $# -eq 0 ] ; then
    REFS="--branches --remotes --tags"
  fi
  git log --graph --decorate --pretty=oneline $REFS $@;
}

gitgh(){
    gitg HEAD "$@"
}

gitgb(){
    gitg HEAD origin/master
}

gitmgrep(){
    local PATTERN="$1";
    git status --porcelain | grep -E '^( |M)M' |  awk '{print $2}' | grep -E -i "$PATTERN"
}

gitd(){
    local PATTERN="$1";
    gitmgrep "$PATTERN" | xargs git diff
}

gitdw(){
    git diff --color-words='[a-zA-Z_]+|[^a-zA-Z_]+' "$@"
}

gitsd(){
    git submodule foreach 'git diff'
}

gita(){
    local PATTERN="$1"
    shift
    git add "$@" $(gitmgrep "$PATTERN") 
}

alias jsonformat="python -m json.tool"

gitcleanall(){
  git clean -dfx
}

function gitgraph(){
    git log --graph --decorate --branches --remotes --tags --name-status HEAD "$@"
}
export -f gitgraph

function gits(){
    COMMAND="git status $@"
    if [ -f .gitmodules ]; then
        git submodule foreach "$COMMAND"
    else
        $COMMAND
    fi
}
export -f gits

function refreshstaging(){
    gits --porcelain | ack '^M' | awk '{print $2}' | xargs git add
}
export -f refreshstaging

function gitsi(){
    git status --ignored
}
export -f gitsi

function globranch(){
  if [ "$#" -ge 1 ]; then
    branches="$@"
  else
    branches="$(git branch | grep '^\*' | awk '{print $2}')"
  fi

  git log --graph --decorate $branches
}
export -f globranch

#move up one directory
alias u='cd ..'

#move down one directory
d(){
  if [ "$(ls -F | grep -e '/$' | wc -l)" = 1 ] ; then
    cd $(ls -F | grep -e '/$')
  else
    echo "[ERROR]More than one directory available to descend to."
  fi
}

#case-insensitive grep
alias grip='grep -i'
#grep for IPv4 addresses
alias grIP="grep -E -e '([[:digit:]]{1,3}\.){3}[[:digit:]]{1,3}' "
#recursive grep from the current directory
alias heregrep='gen_heregrep'

HEREGRIP_FLAGS="-i"
alias heregrip="__heregrep $HEREGRIP_FLAGS"

CHEREGRIP_FLAGS="$HEREGRIP_FLAGS --include '*.c' --include '*.h' --include '*.cpp' --include '*.hpp'"
alias cheregrip="__heregrep $CHEREGRIP_FLAGS"

alias heregrepq='gen_quoted_heregrep'
alias heregripq='EXTRA_HEREGREP_FLAGS="-i" gen_quoted_heregrep'

gen_quoted_heregrep(){
  PATTERN="('|\")$1\1"
  shift
  gen_heregrep $PATTERN "$@"
}


__heregrep(){
  HEREGREP_FLAGS="-r -I -n -R "
  if [ -t 1 ]; then
    gr --color=always ${HEREGREP_FLAGS} "$@" . | lessc
  else
    gr ${HEREGREP_FLAGS} "$@" .
  fi
}

#look for files that match the string (i.e. $2), starting from 'there' (i.e. $1)
therefind(){
  local highlight_regex="$(echo "$2" | sed 's@?@.@g'  | sed 's@*@.*@g')"
  RESULTS="$(find -L $1 -iname "*$2*")"
  CLEAN_RESULTS="$(echo "$RESULTS" | sed 's@^\./@@')"

  if [ -t 1 ]; then
     echo "$CLEAN_RESULTS" | grip -e "$highlight_regex" --color=always |\
      lessc
  else
    echo "$CLEAN_RESULTS"
  fi
}

# Look for files that match the string, starting from 'here' (i.e. cwd)

herefind(){
    therefind . $1
}

ff(){
    FZF_DEFAULT_COMMAND='find .' fzf $@| sed 's@^./@@g' | xclip -r
}

export -f herefind

# Extracts file paths from stdin and outputs them one per line with no repeats
filepaths(){
    ack '(?:\w|/|\.|-)+'  -o | \
        xargs -I myFilename sh -c 'if [ -f myFilename ]; then echo myFilename; fi' | \
        uniq


}


urls(){
  ack 'https?://[a-zA-Z./\-_]+' -o
}

# Gets file paths from last command that was run
lastfiles(){
    fc -s | filepaths
}

cf(){
    local ENTRIES_LIST_FILE=$(mktemp)
    local SELECTION_FILE=$(mktemp)

    fc -s | sed 's@"@\\"@g' | filepaths | xargs -I line echo "\"line\" \"\"" > $ENTRIES_LIST_FILE

    if [ $(cat $ENTRIES_LIST_FILE | wc -l) -gt 1 ]; then
        whiptail --menu "copy file path to clipboard" 0 0 10 $(cat $ENTRIES_LIST_FILE | tr '\n' ' ') 2> $SELECTION_FILE
    else
        cat $ENTRIES_LIST_FILE > $SELECTION_FILE
    fi

    local VAL="$(cat $SELECTION_FILE | tr -d '\n' | cut -d ' ' -f1 | sed 's@"@@g')"

    echo -n $VAL | xclip
    errcho "Copied $VAL into clipboard"
    rm $ENTRIES_LIST_FILE
    rm $SELECTION_FILE
}
export cf

lasturls(){
  fc -s | urls
}


# Opens all files that were shown in the output of the last command into
# vim tabs
vimfall(){
        LAST_FILES="$(mktemp)"
        lastfiles | tr '\n' ' ' > $LAST_FILES
        vim -p $(cat $LAST_FILES)
}

# Opens VIM reading from stdin and using syntax highlight type specified by first param
vimpipe(){
    SYNTAX_TYPE="$1"
    vim - -c "set syntax=$SYNTAX_TYPE | command Q q!"
}

# 'less' pager, but with color, line numbers and closing if the output fits the screen
lessc(){
  less -R -N -X -F "$@"
}
export -f lessc

lesside(){
  less -S -# 4 "$@"
}

treec(){
  tree -C $@ | lessc
}

# ======== BACKGROUND VIMS ================
#add parent vim counter to the bash prompt. Those are the vim processes put on
#hold via :sh
countParentVims(){
  P_ID=$1
  VIM_COUNT=0

  while [ $P_ID != 1 ] ; do
    P_INFO=$(ps -o comm=,ppid= $P_ID )
    P_ID="$(echo $P_INFO | awk '{print $2}' )";
    P_NAME="$(echo $P_INFO | awk '{print $1}' )"
    if echo "${P_NAME}" | grep -qE '\bvim?|nvim\b' ; then
        VIM_COUNT=$(expr $VIM_COUNT + 1)
    fi
  done
  echo $VIM_COUNT
}

setVimJobsPrompt(){
  NUM_VIM_JOBS="$(jobs | grep -E '\bvim?|nvim\b' | wc -l)"
  if [ "$NUM_VIM_JOBS" -gt 0 ] ; then
    if [ -z "$OLD_PS1" ]; then
      OLD_PS1="$PS1"
    fi

    PS1="\[\033[34m\](vim jobs[$NUM_VIM_JOBS])\[\033[0m\]$OLD_PS1"
  elif [ -n "$OLD_PS1" ]; then
    PS1="$OLD_PS1"
  fi
}

if [ -n "$VIM"  ] ; then
  NUM_VIMS=$(countParentVims $$)
  PS1="\[\033[34m\](vim[$NUM_VIMS])\[\033[0m\]$PS1"
fi


PROMPT_COMMAND="$PROMPT_COMMAND setVimJobsPrompt ;"

#========= END BACKGROUND VIMS ============A

#adds timestamp to PS1 // this is messing up long lines
PS1="\D{%T}$PS1"

alias viml='vim_gotoline'
vim_gotoline(){
  PROCESSED_ARGS="tab all"
  for arg in "$@"; do
    #echo "arg: <<<$arg>>>"
    LINE_NUM="$(echo "$arg" | grep -E -o -e ':[0-9]+:?' | tr -d ':' )"
    #echo "Got any line num? :     $LINE_NUM"

    if [ -n "$LINE_NUM" ] ; then
      #echo "Processing this arg: $arg"
      FILE_NAME="$(echo "$arg" |  cut -d: -f1)"
      #echo "Which yielded this FILE_NAME: $FILE_NAME"
    else
      LINE_NUM=1
      FILE_NAME="$arg"
    fi
    PROCESSED_ARGS="${PROCESSED_ARGS} |tabnew $FILE_NAME | $LINE_NUM "
  done
  PROCESSED_ARGS="$PROCESSED_ARGS | tabn | q"
  vim -c "$PROCESSED_ARGS"
}

vimtabs(){
  vim -c 'tab all' "$@"
}


#add a (ssh) prefix to the bash prompt when using a remote shell
IS_SSH_SESSION=false
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
  IS_SSH_SESSION=true
fi
if $IS_SSH_SESSION ; then
  PS1="\[\033[31m\](ssh)\[\033[0m\]$PS1"
fi

if [ -n "$SHUTILS_DISK_PART_EDIT" ]; then
	PS1="[EDIT YOUR PARTION]$PS1"
fi

errcho(){
    >&2 echo "$@"
}

#propagate these utils to the remote ssh session
rcssh(){
  USER_AT_HOST=$1

  echo "Enter password for ${USER_AT_HOST}"
  read -s MYPASS
  SP="sshpass -p ${MYPASS}"
  SSHP="$SP ssh $USER_AT_HOST"

  #test ability to copy stuff into remote
  echo "[INFO]Copying bashrc_utils.sh into remote..."
  if ! $SP scp ${BASHUTIL_INST_DIR}/codeToAppend.sh ${BASHUTIL_INST_DIR}/bashrc_utils.sh  $USER_AT_HOST:/tmp ; then
    errcho "[ERROR]Failed to copy file to remote."
    return 1
  fi

  echo "[INFO]Removing old source line from bashrc if it exists..."
  if ! $SSHP sed -i '/^.*#delete_me!!$/d' \$HOME/.bashrc; then
    errcho "[ERROR]Failed removing old source line from bashrc"
    return 2
  fi

  if ! $SSHP "cat \$HOME/.bashrc /tmp/codeToAppend.sh > /tmp/enhancedBashrc.sh"; then
    errcho "[ERROR]Could not concatenate remote bashrc with custom bashrc"
    return 3
  fi

  if ! $SSHP cp /tmp/enhancedBashrc.sh \$HOME/.bashrc ; then
    errcho "[ERROR]Could not copy custom bashrc to remote ~/.bashrc"
    return 4
  fi

  sshpass -p $MYPASS ssh $USER_AT_HOST -X
}

#create sh script file
shstub(){
  echo "#!/bin/sh" > $1 && chmod +x $1
}

#create C stub file
cstub(){
  echo "#include <stdio.h>" > $1
  echo "int main(int arc, char **argv){" >> $1
  echo "  return 0;" >> $1
  echo "}" >> $1
}

#compile and run .c files
comprun(){
  local executable="my_compiled_file"
  rm -f "$executable"

  if ls *.c 2> /dev/null > /dev/null ; then
    local COMPILER="gcc"
    local SOURCES="*.c"
  fi

  if ls *.cpp 2>/dev/null 1> /dev/null; then
    local COMPILER="g++"
    local SOURCES="*.cpp"
  fi

  if [ -z "$COMPILER" ] ; then
    errcho "No .c or .cpp files found"
    return 1
  fi

  $COMPILER $SOURCES -Wall -Wextra -g -o "$executable" && ./${executable}
}

#extract files from rpm
exrpm(){
  rpm2cpio $1 > a.cpio && mkdir tree && cd tree && cpio -idv < ../a.cpio
}

isYes(){
  if echo "$1" | grep -q -E -i '^ *y(es)? *$' ; then
    return 0
  fi
  return 1
}

isNo(){
  if echo "$1" | grep -q -E -i '^ *no? *$' ; then
    return 0
  fi
  return 1
}

#Outputs the chosen string form a set of options. If the user enters an empty
#  string, then output the default value.
#Use this function like so:
#  MY_VAR=$(politeSetValue help_text default opt2 opt3)
politeSetValue(){
  local HELP_TEXT="$1"
  local DEFAULT="$2"
  shift 2
  local VALID_VALUE="false"
  local RESP=""
  local OPTIONS="$DEFAULT"
  for opt in "$@" ; do
    OPTIONS="${OPTIONS}|${opt}"
  done

  while ! $VALID_VALUE ; do
    >&2 echo -ne "$HELP_TEXT (${OPTIONS})(default: $DEFAULT) "
    read RESP
    if [ -z "$RESP" ] ; then
      RESP="$DEFAULT"
      VALID_VALUE='true'
    fi
    for opt in $DEFAULT "$@" ; do
      [ "$RESP" = "$opt" ] &&  VALID_VALUE="true" && break
    done
  done
  echo "$RESP"
}

########## Anotate files #############
annotate(){
  FNAME=$1
  OLD_COMMENT="$(getfattr -n user.comment "$FNAME" 2>/dev/null | \
                 grep 'user\.comment' | \
                 sed 's@user\.comment="@@' | sed 's@"$@@')"
  OLD_COMMENT="$(echo -e "$OLD_COMMENT")"
  TMP_FILE=$(mktemp)
  echo "$OLD_COMMENT" > $TMP_FILE
  vim $TMP_FILE
  if grep "$TMP_FILE" -E -e '[0-9a-zA-Z]' -q ; then
    setfattr -n user.comment -v "$(cat $TMP_FILE)" "$FNAME"
  else
    setfattr -x user.comment "$FNAME" 2>/dev/null
  fi
}

lsa(){
  ${BASHUTIL_INST_DIR}/lsa.py
}


#useful in order to compile stuff with uninstalled libs:
#export CPLUS_INCLUDE_PATH="/home/tomaz/includes/HiStorAPI"
#export LIBRARY_PATH="/home/tomaz/libs/HiStorAPI"
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

#kill qemu
kemu(){
 kill -2 $(psg qemu | awk '{print $2 " " $8}' | grep qemu | awk '{print $1}')
}

#Add tab complete in interactive python
#if [ ! -f ~/.pythonrc ] ; then
#  echo "#Auto-created by ${BASH_SOURCE[0]}"           >> ~/.pythonrc
#  echo "try:"                                         >> ~/.pythonrc
#  echo "    import readline"                          >> ~/.pythonrc
#  echo "except ImportError:"                          >> ~/.pythonrc
#  echo "    print('Module readline not available.')"  >> ~/.pythonrc
#  echo "else:"                                        >> ~/.pythonrc
#  echo "    import rlcompleter"                       >> ~/.pythonrc
#  echo '    readline.parse_and_bind("tab: complete")' >> ~/.pythonrc
#fi
#export PYTHONSTARTUP=~/.pythonrc


pythonjson(){
    COMMAND=$@
    PREAMBLE="import json; import sys; js = json.loads(sys.stdin.read());"
    python -c "${PREAMBLE}${COMMAND}"
}

#reminder notes...
REMINDER_FILE="${HOME}/.my_reminders"
_show_reminders(){
  if [ ! -e "$REMINDER_FILE" ] ; then
    return
  fi

  if [ "$(wc -c $REMINDER_FILE | awk '{print $1}')" -eq 0 ] ; then
    return
  fi

  echo -e "${YELLOW_ESCAPE}====== REMINDERS!(from ${REMINDER_FILE}) ======${END_COLOR_ESCAPE}"
  cat $REMINDER_FILE | sed 's/^/===> /g'
}

remindme(){
  if [ $# -gt 1 ] ; then
    errcho "Usage: remindme [ -e | message ]"
    errcho "Remember to quote your reminder if it has multiple words."
    return 1
  fi

  if [ "$#" -eq 1 ] ; then
    if [ "$1" = "-e" ] ; then
      editor "$REMINDER_FILE"
    else
      local note="$1"
      echo "[INFO]Appending note to $REMINDER_FILE"
      echo "$(date +"%d/%b/%y %H:%M") $note" >> $REMINDER_FILE
    fi
  else #remindme called with no parameters
    _show_reminders
    return
  fi
}

psg(){
    local ps_command="ps -eo pid,ppid,pgid,user,start,ignored,args"
    [ -t 1 ] && echo -en "${YELLOW_ESCAPE}"
    $ps_command | head -n 1
    [ -t 1 ] && echo -en "${END_COLOR_ESCAPE}"
    $ps_command | tail -n +2 | gr "$@"
}

recover_vim(){
  local FILELIST="$(mktemp)"
  find . -iname '.*.sw?' > $FILELIST
  vimtabs $(python  <<EOF | tr '\n' ' '
for line in open("$FILELIST").readlines():
  components = line.split('/')
  components[-1] = components[-1][1:-5]
  if components[0] == '.':
    components = components[1:]
  print '/'.join(components)
EOF
)
  echo "Delete all these files?"
  cat $FILELIST
  echo -n "(yes|no)"
  read ANSWER
  if isYes "$ANSWER" ; then
    cat $FILELIST | xargs -L1 rm -v
  fi
}

alias ctags="ctags -f mytagsfile.ignore"

#create a disk  image with a FAT filesystem:
create_disk_image(){
  SIZE="$1" #in megs
  if [ -z "$SIZE" ]; then
    SIZE=100
  fi

  FSTYPE="$2"
  if [ -z "$FSTYPE" ]; then
    FSTYPE="fat32"
    FORMAT_PROG="mkfs.vfat -F32"
  else
    FORMAT_PROG="mkfs.${FSTYPE}"
  fi

  dd if=/dev/zero of=mydisk.img bs=1M count=100
  parted mydisk.img mklabel msdos
  echo "Ignore" | parted mydisk.img mkpart primary fat32 512B 100MB
  dd if=mydisk.img of=mypart.img bs=512 skip=1
  $FORMAT_PROG mypart.img
  dd if=mypart.img of=mydisk.img bs=512 seek=1 conv=notrunc
  rm mypart.img
  MOUNT_DIR="$(mktemp -d)"
  sudo mount -o loop,offset=512 mydisk.img $MOUNT_DIR
  pushd $MOUNT_DIR
  SHUTILS_DISK_PART_EDIT=true bash
  sync;
  popd
  sudo umount $MOUNT_DIR
}

#run on shell startup-------------------------
if [ -z "$VIM"  ] ; then
  remindme
  date
fi

dpkgrep(){
  if [ -t 1 ]; then
    local HEADER="$(dpkg -l | head -n5)"
    echo -e "${YELLOW_ESCAPE}${HEADER}${END_COLOR_ESCAPE}"
  fi
  dpkg -l | tail -n +5 | ack --nopager "$@"
}

streamDesktopAudio(){
    pactl list | grep "Monitor Source" | sed 's/Monitor\ Source:\ /pulse:\/\//' | \xargs cvlc --sout '#transcode{acodec=flac,ab=512,channels=2}:standard{access=http,dst=0.0.0.0:8888/pc.mp3}' -vvv -
}


alias config_kb_autorepeat="xset r rate 300 50"
cot(){
    highlight -O xterm256 "$@"
}

ackb(){
    PATTERN="$1"
    shift
    ack "\\b$PATTERN\\b" "$@"
}

agdef(){
    local REGEX="$1"
    shift
    ag "\b(class|def|function|interface|enum|type|var|const|val) *${REGEX}\b" $@
}

editbashrc(){
    vim ~/.bashrc
}

pyact(){
    conda deactivate && source $1/bin/activate
}


tabletmap(){
    xinput | ag wacom | ag 'id=\d+' -o | cut -d= -f 2 | xargs -t -I id xinput map-to-output id HDMI-2
}
