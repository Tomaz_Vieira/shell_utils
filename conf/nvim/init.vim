set nocompatible
scriptencoding utf-8
set encoding=utf-8
set nu
syntax on

set directory=$HOME/.vim/swapfiles//

"autocmd BufReadPost * :DetectIndent
set tabstop=2
set backspace=2
set backspace=indent,eol,start

set listchars=tab:▸-,eol:¬,space:⬚

set ffs=unix,dos,mac
set colorcolumn=120

set expandtab
set shiftwidth=4
set tabstop=4


colorscheme default
"highlight SpecialKey ctermfg=7
"colorscheme slate
highlight LineNr ctermbg=237
highlight ColorColumn ctermbg=234
"highlight SpecialKey ctermfg=243
hi MatchParen cterm=underline ctermfg=NONE ctermbg=NONE
highlight Comment term=bold ctermfg=29

"highlight Comment ctermfg=blue

set list


au BufRead,BufNewFile *.conf set filetype=cfg

"Saves last position in file so they can be reopened where I left off
au BufReadPost *
\ if line("'\"") > 1 && line("'\"") <= line("$") && &ft !~# 'commit'
\ |   exe "normal! g`\""
\ | endif

" command DeleteFirst 1delete
command DelTrailingSpaces %s/\v( |\t)+$//g
command MouseCopy set nonu | set nolist
command HLS set hlsearch!

" new windows open to the right
set splitright
set splitbelow

" ==== TAGS STUFF ==========

set tags=./mytagsfile.ignore,mytagsfile.ignore;

" more shortcuts for vtags
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
"Ctrl+\ - Open the definition in a new tab
map <A-]> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>
"Alt+] - Open the definition in a vertical split
map <A-[> :sp <CR>:exec("tag ".expand("<cword>"))<CR>
"Alt+[ - Open the definition in a horizontal split

" ========= END TAGS STUFF =========

map <C-o> :!ag "\b(class\|def\|function\|interface\|enum\|type\|var\|const\|val\|as) *<cword>\b\|^ *<cword> *\(" ; ag "\b(class\|def\|function\|interface\|enum\|type\|var\|const\|val\|as) *<cword>\b\|^ *<cword> *\(" -l \| xclip -r<CR>
map <C-i> :!ag "\b<cword> *="<CR>
nnoremap <Up> gk
nnoremap <Down> gj

" show tabs even if there's only a single document open
set showtabline=2

"Hit ESC to clear search highlights
"nnoremap <esc> :noh<return><esc>

"Searching and regexes
set ignorecase "search is case insensitive...
set smartcase           " ... unless the query has capital letters.
set magic "enables extended regex, i think

"Disable mouse so i can copy paste
set mouse=




command PL !pylint %
command MP !mypy --disallow-incomplete-defs --disallow-untyped-defs %


"syntastic recommended newbie configs
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0
