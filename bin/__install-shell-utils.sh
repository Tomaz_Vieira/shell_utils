#!/bin/bash
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")


source $SCRIPTPATH/../bashrc_utils.sh

BASHRC_INSTALL_ANCHOR="#<<<SHELLUTILS>>>"

echo "Enter directory where you want to install shell_utils."
echo "Defaults to $HOME/shell_utils."
echo -n "Install shell_utils at: "
read INSTALL_DIR

if [ -z "$INSTALL_DIR" ]; then
    INSTALL_DIR="$HOME/shell_utils"
fi

echo "Installing shell utils at $INSTALL_DIR"

if [ -d $INSTALL_DIR ]; then
    echo 1>&2 "$INSTALL_DIR already exists!"
    exit 1
fi


mkdir -p $INSTALL_DIR
exit 3
git clone https://bitbucket.org/Tomaz_Vieira/shell_utils $INSTALL_DIR
