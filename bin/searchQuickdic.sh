#!/bin/bash
if [ $1 = '-i' ] ; then
    echo "Enter german word to search for:"
    read WORD
    SEARCH_WORD="$(echo "$WORD" | sed 's@a:@ä@g' | sed 's@u:@ü@g' | sed 's@o:@ö@g' | sed 's@s:@ß@g' | iconv -t ISO-8859-1)"
else
    SEARCH_WORD="$(xclip -out -selection primary | iconv -t ISO-8859-1 )"
fi
curl 'http://www.quickdic.org/cgi-bin/quickdic.exe' -H 'Origin: http://quickdic.org' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.8' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Referer: http://quickdic.org/Contents/form_e.html' -H 'Connection: keep-alive' --data "opt=svn&opt1=d&str=${SEARCH_WORD}&opt=x&opt=i&opt=g&opt=k&opt2=1" --compressed | elinks
